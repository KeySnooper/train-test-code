import os, uuid
from flask import Flask, request, redirect, render_template
from threading import Thread


UPLOAD_FOLDER = './'
ALLOWED_EXTENSIONS = set(['wav', 'txt'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

data = "test"
new_data_available = False


@app.route('/')
def index():
    return render_template("view_text.html")


@app.route('/data', methods=['GET'])
def get_data():
    return data


@app.route('/process', methods=['POST'])
def upload_file():
    """upload sensor file of single character"""
    # check if the post request has the file part
    if 'file' not in request.files:
        return redirect(request.url)
    afile = request.files['afile']
    gfile = request.files['gfile']
    filename = uuid.uuid4()
    if afile and gfile:
        afile.save(os.path.join(app.config['UPLOAD_FOLDER'], "a_"+filename))
        gfile.save(os.path.join(app.config['UPLOAD_FOLDER'], "g_"+filename))
        thread = Thread(target=process, args=(filename,))
        thread.start()
        return None


def process(filename):
    pass


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
