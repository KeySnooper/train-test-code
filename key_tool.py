#!/usr/bin/python3.4

import os
import wave
import argparse
import shutil
from subprocess import call


parser = argparse.ArgumentParser()
parser.add_argument("action")
parser.add_argument("subaction", nargs='?')
args = parser.parse_args()
print ("Running tool...")

if args.action == "wav":
    for file in os.listdir('.'):
        if not str(file).endswith('.pcm'):
            continue
        with open(file, 'rb') as pcmfile:
            pcmdata = pcmfile.read()
        with wave.open(str(file).split('.')[0]+'.wav', 'wb') as wavfile:
            wavfile.setparams((1, 2, 44100, 0, 'NONE', 'NONE'))
            wavfile.writeframes(pcmdata)
elif args.action == "data":
    print ("arranging data")

    call(["key_tool", "wav"])
    os.makedirs("pcms")

    for file in os.listdir('.'):
        if not os.path.isfile(file):
            continue

        if file.endswith(".pcm"):
            shutil.move(file, "pcms/" + file)
            continue

        key = file.split('_')[2]
        if not os.path.exists(key):
            os.makedirs(key+"/acc", exist_ok=True)
            os.makedirs(key+"/gyro/x", exist_ok=True)
            os.makedirs(key+"/gyro/y", exist_ok=True)
            os.makedirs(key+"/gyro", exist_ok=True)
            os.makedirs(key+"/audio", exist_ok=True)

        if file.startswith("acc_"):
            shutil.move(file, key+"/acc/"+file)
        elif file.startswith("gyrox_"):
            shutil.move(file, key+"/gyro/x/"+file)
        elif file.startswith("gyroy_"):
            shutil.move(file, key+"/gyro/y/"+file)
        elif file.startswith("audio_"):
            shutil.move(file, key+"/audio/"+file)

    if args.subaction and args.subaction == "clean":
        pass
    elif args.subaction and args.subaction == "renumber":
        pass

elif args.action == "merge":
    pass
