import numpy as np
from sklearn.base import BaseEstimator


class FusedModel(BaseEstimator):
    """
    A Classifier to combine various classifiers, using weights or offsets
    """

    def __init__(self, models=None, weights=None, mode="weight"):
        self.num_models = len(models)
        self.models = models
        self.mode = mode
        self.weights = weights

    def set_models(self, models):
        """
        set all models
        :param models: models
        """
        self.models = models

    def set_mode(self, weights, mode):
        """
        set mode and weights
        :param weights: list of weights
        :param mode: "weight" or "offset" or "append"
            weight -> will run both classifiers separately and will find the result based on a
            weighted average of their propablity of prediction
            offset ->
            append -> append both features and make a new feature vector

        """
        self.mode = mode
        self.weights = weights

    def _append_features(self, X):
        """
        append features
        :param X: list of feature set [[feature set 1], [feature set 2]]
        :return: appended feature set
        """
        X_new = [np.array([]) for x in X[0]]
        for feature_set in X:
            for i in range(len(feature_set)):
                np.append(list(feature_set[i]), X_new[i])
        return X_new

    def fit(self, X, Y, input_split=False):
        if self.mode == "append":
            X = self._append_features(X)
            Y = Y[0]
            self.models[0].fit(X, Y)
            return

        if input_split or self.mode == "append":
            initial_X = X
            X = [[] for x in initial_X[0]]
            for sample_tuple in initial_X:
                for i in range(len(sample_tuple)):
                    X[i].append(sample_tuple[i])
            initial_Y = Y
            Y = [[] for x in initial_Y[0]]
            for sample_tuple in initial_Y:
                for i in range(len(sample_tuple)):
                    Y[i].append(sample_tuple[i])

        for x, y, model in zip(X, Y, self.models):
            model.fit(x, y)

    def predict(self, X):
        """
        classify X
        :param X: sample list for each model, len(X) == len(model)
        :return: list of predicted classes
        """
        if self.mode != "append" and len(X) != self.num_models:
            raise Exception("models and data dimension mismatch")
        predicted_y = []

        if self.mode == "weight":

            X = [m.predict_log_proba(x) for m, x in zip(self.models, X)]
            predicted_x = []
            for sample_idx in range(len(X[0])):
                samples_per_model = []
                for model_idx in range(len(X)):
                    samples_per_model.append(X[model_idx][sample_idx])
                predicted_x.append(samples_per_model)

            for current_char_feature in predicted_x:
                # loop for each char
                y_tuples_weighted = []
                for class_idx in range(len(self.models[0].classes_)):
                    weighted_prob = 0
                    current_class = self.models[0].classes_[class_idx]
                    for model_idx in range(len(current_char_feature)):
                        weighted_prob += self.weights[model_idx]*current_char_feature[model_idx][class_idx]
                    y_tuples_weighted.append((current_class, weighted_prob))
                y_tuples_weighted.sort(key=lambda x: x[1], reverse=True)
                predicted_y.append(y_tuples_weighted[0][0])

        elif self.mode == "append":
            X = self._append_features(X)
            return self.models[0].predict(X)
        elif self.mode == "offset":
            X = [predict_with_prob(m, x) for m, x in zip(self.models, X)]
            predicted_x = []
            for sample_idx in range(len(X[0])):
                samples_per_model = []
                for model_idx in range(len(X)):
                    samples_per_model.append(X[model_idx][sample_idx])
                predicted_x.append(samples_per_model)
            for current_char_feature in predicted_x:
                y_prob_matrix = [(x[0], x[1]+w) for x, w in zip(current_char_feature, self.weights) ]
                y_prob_matrix.sort(key=lambda x: x[1], reverse=True)
                predicted_y.append(y_prob_matrix[0][0])
        else:
            raise Exception("mode not set")
        return predicted_y

    def score(self, X, Y, input_split=False):
        """
        test on X,Y and get accuracy
        :param X: list of samples for each model; len(X) == len(models)
        :param Y:  list of samples outputs for each model; len(X) == len(models)
        :param input_split: if len(x),len(x) == len(samples)
        :return: score
        """
        if input_split:
            initial_X = X
            X = [[] for x in X[0]]
            for sample_tuple in initial_X:
                for i in range(len(sample_tuple)):
                    X[i].append(sample_tuple[i])
            initial_Y = Y
            Y = [[] for x in initial_Y[0]]
            for sample_tuple in initial_Y:
                for i in range(len(sample_tuple)):
                    Y[i].append(sample_tuple[i])

        predicted_y = self.predict(X)

        final_score = sum([1 for g_y, p_y in zip(Y[0], predicted_y) if g_y == p_y])/float(len(predicted_y))
        # print final_score
        return final_score

    def crossval(self, X, Y, cv):
        """
        cross validate using this model
        :param X: list of samples for each model [ [for model1],[for model 1], ... ]
        :param Y: list of samples outputs for each model [ [for model1],[for model 1], ... ]
        :param cv: number of folds
        :return: list
        """
        cv_len = len(X[0])/cv
        final_prob = []

        for x, y in zip(X, Y):
            np.random.seed(0)
            np.random.shuffle(x)
            np.random.seed(0)
            np.random.shuffle(y)

        for k in range(cv):
            x_splitted_test = []
            y_splitted_test = []
            x_splitted_train = []
            y_splitted_train = []
            for i in range(len(self.models)):
                x_splitted_test.append(X[i][k*cv_len: (k+1)*cv_len])
                y_splitted_test.append(Y[i][k*cv_len: (k+1)*cv_len])
                x_splitted_train.append(list(X[i][:k*cv_len]) + list(X[i][(k+1)*cv_len:]))
                y_splitted_train.append(list(Y[i][:k*cv_len]) + list(Y[i][(k+1)*cv_len:]))
            self.fit(x_splitted_train, y_splitted_train)
            final_prob.append(self.score(x_splitted_test, y_splitted_test))
        return final_prob
