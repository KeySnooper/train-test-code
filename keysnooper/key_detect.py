import scipy
import numpy as np

KEY_WIDTH = 0.36
SPEC_THRESHOLD = 1e-06
METHOD = "spectrogram"


def detect(in_queue, out_queue, display_queue, config):
    """

    :param in_queue: path to files
    :param out_queue: key positions
    :param display_queue:
    :param config:
    :return:
    """
    for data in in_queue:
        if METHOD == "spectrogram":
            a = scipy.signal.spectrogram(data[0], data[1])
            spec_acc = np.sum(a[2], axis=0)
            t = a[1]
            tt = [float(t[np.where(spec_acc == y)]) for y in [x for x in spec_acc if x > SPEC_THRESHOLD]]
            clips = []
            while len(tt) > 0:
                clips.append((tt[0], tt[0]+KEY_WIDTH))
                tt = filter(lambda i: i > tt[0]+KEY_WIDTH, tt)
            return clips
        else:
            return None
