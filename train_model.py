import argparse
import importlib
import os
from multiprocessing import Process
from multiprocessing import Queue
import itertools
import numpy as np
from sklearn.externals import joblib
from sklearn.feature_selection import RFECV
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler

from keysnooper.fused_estimator import FusedModel


NUM_AUDIO_FEATURES = 193  # 193
NUM_GYRO_FEATURES = 180  # 180
NUM_ACC_FEATURES = NUM_GYRO_FEATURES
NUM_SAMPLES = 17

ACCEPTED_TYPES = ["audio", "acc", "gyro"]
AUDIO_WEIGHT = 0.9
GYRO_WEIGHT = 0.1

MODE = 'cross'  # cross, test


def parse_audio_files(dir_path, label):
    """
    parse all files in directories and return features
    :param dir_path:
    :param label:
    :return: features
    """

    features, labels = np.empty((NUM_SAMPLES, NUM_AUDIO_FEATURES)), np.empty(NUM_SAMPLES, dtype=str)
    for fn in os.listdir(dir_path):
        index = int(fn.split('_')[-1].split('.')[0])
        if index > NUM_SAMPLES:
            continue
        try:
            mfccs, chroma, mel, contrast, tonnetz = helper.extract_audio_feature(os.path.join(dir_path, fn))
        except Exception as e:
            print ("Error encountered while parsing file: " + str(fn))
            raise e
            # continue
        # print (len(mfccs), len(chroma), len(mel), len(contrast), len(tonnetz),
        # len([mfccs, chroma, mel, contrast, tonnetz]) )
        ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
        features[index-1] = ext_features
        labels[index-1] = label
    return np.array(features), np.array(labels)


def parse_acc_files(dir_path, label):
    """
    parse all sensor files of a class
    :param dir_path: path to class dir
    :param label: label of class
    """
    features, labels = np.empty((NUM_SAMPLES, NUM_ACC_FEATURES)), np.empty(NUM_SAMPLES, dtype=str)
    for fn in os.listdir(dir_path):
        index = int(fn.split('_')[-1].split('.')[0])
        if index > NUM_SAMPLES:
            continue
        try:
            mfccs, chroma, mel = helper.extract_sensor_features(os.path.join(dir_path, fn))
        except Exception as e:
            print ("Error encountered while parsing file: ", fn)
            print (e)
            continue
        ext_features = np.hstack([mfccs, chroma, mel])
        features[index-1] = ext_features
        labels[index-1] = label
    return np.array(features), np.array(labels)


def parse_gyro_files(dir_path, label):
    """
    parse all sensor files of a class
    :param dir_path: path to class dir
    :param label: label of class
    """
    features, labels = np.empty((NUM_SAMPLES, NUM_GYRO_FEATURES*2)), np.empty(NUM_SAMPLES, dtype=str)

    for fn in os.listdir(dir_path+"/x"):
        index = int(fn.split('_')[-1].split('.')[0])
        if index > NUM_SAMPLES:
            continue
        xfile = os.path.join(os.path.join(dir_path, "x"), "gyrox"+fn[5:])
        yfile = os.path.join(os.path.join(dir_path, "y"), "gyroy"+fn[5:])
        try:
            xmfccs, xchroma, xmel = helper.extract_sensor_features(xfile)
            ymfccs, ychroma, ymel = helper.extract_sensor_features(yfile)
        except Exception as e:
            print (" Error encountered while parsing file: ", fn)
            print (e)
            continue

        ext_features = np.hstack([xmfccs, xchroma, xmel, ymfccs, ychroma, ymel])
        features[index-1] = ext_features
        labels[index-1] = label
    return np.array(features), np.array(labels)


def merge_mapped_features(features1, features2):
    return [np.vstack([features1[0], features2[1]]), np.append(features1[1], features2[1])]


def parse_files(parent_dir, file_types=None, exclude_dir=None):
    """
    loop thorough class in data dir and call corresponding fun for each type
    :param parent_dir:
    :param file_types:
    :param exclude_dir:
    :return: feature dict
    """
    features = {"audio": [np.empty((0, NUM_AUDIO_FEATURES)), np.empty(0)],
                "gyro": [np.empty((0, NUM_GYRO_FEATURES*2)), np.empty(0)],
                "acc": [np.empty((0, NUM_ACC_FEATURES)), np.empty(0)]}
    for class_dir in os.listdir(parent_dir):
        if exclude_dir and class_dir in exclude_dir:
            continue

        print ("process " + class_dir)

        if "audio" in file_types:
            audio_features, labels = parse_audio_files(os.path.join(os.path.join(parent_dir, class_dir),
                                                                    "audio"), str(class_dir))
            features["audio"] = merge_mapped_features(features["audio"], [audio_features, labels])
        if "gyro" in file_types:
            gyro_features, labels = parse_gyro_files(os.path.join(os.path.join(parent_dir, class_dir),
                                                                  "gyro"), str(class_dir))
            features["gyro"] = merge_mapped_features(features["gyro"], [gyro_features, labels])
        if "acc" in file_types:
            acc_features, labels = parse_acc_files(os.path.join(os.path.join(parent_dir, class_dir),
                                                                "acc"), str(class_dir))
            features["acc"] = merge_mapped_features(features["acc"], [acc_features, labels])
    return features


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('training_data', nargs='+', type=str, help='List of files and folders containing training data')
    parser.add_argument('output_file', type=str, help='Path of the resulting trained model')
    parser.add_argument('--features', '-f', default='MFCC', help='Feature extraction class')
    parser.add_argument('--no_scaling', action='store_true', help='Whether to scale features between [0,1] (suggested)')
    parser.add_argument('--folds', type=int, default=5, help='How many folds to use for cross-validation')
    parser.add_argument('--no_fused', action='store_true', help='Whether to use fused model')
    parser.add_argument('--classifier', '-c', nargs=2, default=['LogisticRegression', 'sklearn.linear_model'],
                        help='Class name and package name of classifier')
    args = parser.parse_args()

    mapped_features = {"audio": [np.empty((0, NUM_AUDIO_FEATURES)), np.empty(0)],
                       "gyro": [np.empty((0, NUM_GYRO_FEATURES*2)), np.empty(0)],
                       "acc": [np.empty((0, NUM_ACC_FEATURES)), np.empty(0)]}

    if args.load_features:
        all_data = joblib.load(args.load_features)
    else:
        # get features for from all the folders
        for f_name in args.training_data:
            t_mapped_features = parse_files(f_name, ACCEPTED_TYPES, exclude_dir=['pcms'])
            mapped_features["audio"] = merge_mapped_features(mapped_features["audio"], t_mapped_features["audio"])
            mapped_features["gyro"] = merge_mapped_features(mapped_features["gyro"], t_mapped_features["gyro"])
            mapped_features["acc"] = merge_mapped_features(mapped_features["acc"], t_mapped_features["acc"])

    if args.save_features:
        joblib.dump(mapped_features, args.save_features)

    # Load pipeline steps
    # 1 - Feature extraction
    pipelines_types = ACCEPTED_TYPES + ["fused"]
    pipelines = dict()
    for t in pipelines_types:
        pipeline = []
        if not args.no_scaling:
            pipeline.append(('Scaler', MinMaxScaler()))
        # 2 - Feature selector and classifier
        classifier = getattr(importlib.import_module(args.classifier[1]), args.classifier[0])()
        if not args.no_feature_selection:
            pipeline.append(('Feature Selection',
                             RFECV(classifier, step=f_X.shape[1] / 10, cv=args.folds, verbose=0)))
        if t == "fused":
            pipeline.append(('Classifier', FusedModel([classifier, classifier], [AUDIO_WEIGHT, GYRO_WEIGHT], "weight")))
        else:
            pipeline.append(('Classifier', classifier))
        pipelines[t] = Pipeline(pipeline)

    print ("Training...")
    for t in pipelines_types:
        if t == "fused":
            pipelines[t].fit([mapped_features["audio"][0], mapped_features["gyro"][0]],
                             [mapped_features["gyro"][1], mapped_features["gyro"][1]])
            print ("Estimating accuracy...")
            print (np.mean(cross_val_score(pipelines[t], [mapped_features["audio"][0], mapped_features["gyro"][0]],
                                           [mapped_features["gyro"][1], mapped_features["gyro"][1]], cv=args.folds + 1)))
        else:
            pipelines[t].fit(mapped_features[t][0], mapped_features[t][1])
            print ("Estimating accuracy...")
            print (np.mean(cross_val_score(pipelines[t], f_X, f_y, cv=args.folds + 1)))

        print ("Writing model to disk")
        joblib.dump(pipelines[t], args.output_file+t)
