import os
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn import svm
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.externals import joblib
from sklearn.base import BaseEstimator
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPClassifier

from scipy.fftpack import rfft

from IPython import embed

import helper

TRAIN_DATA_DIR = "../data/lab/4"
TEST_DATA_DIR = "../data/lab/4"
NUM_AUDIO_FEATURES = 193  # 193
NUM_GYRO_FEATURES = 180  # 180
NUM_ACC_FEATURES = NUM_GYRO_FEATURES
NUM_SAMPLES = 17

MODE = 'cross'  # cross, test


def parse_audio_files(dir_path, label):
    """
    parse all files in directories and return features
    :param dir_path:
    :param label:
    :return: features
    """

    features, labels = np.empty((NUM_SAMPLES, NUM_AUDIO_FEATURES)), np.empty(NUM_SAMPLES, dtype=str)
    for fn in os.listdir(dir_path):
        index = int(fn.split('_')[-1].split('.')[0])
        if index > NUM_SAMPLES:
            continue
        try:
            mfccs, chroma, mel, contrast, tonnetz = helper.extract_audio_feature(os.path.join(dir_path, fn))
        except Exception as e:
            print ("Error encountered while parsing file: " + str(fn))
            raise e
            # continue
        # print (len(mfccs), len(chroma), len(mel), len(contrast), len(tonnetz),
        # len([mfccs, chroma, mel, contrast, tonnetz]) )
        ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
        features[index-1] = ext_features
        labels[index-1] = label
    return np.array(features), np.array(labels)


def parse_acc_files(dir_path, label):
    """
    parse all sensor files of a class
    :param dir_path: path to class dir
    :param label: label of class
    """
    features, labels = np.empty((NUM_SAMPLES, NUM_ACC_FEATURES)), np.empty(NUM_SAMPLES, dtype=str)
    for fn in os.listdir(dir_path):
        index = int(fn.split('_')[-1].split('.')[0])
        if index > NUM_SAMPLES:
            continue
        try:
            mfccs, chroma, mel = helper.extract_sensor_features(os.path.join(dir_path, fn))
        except Exception as e:
            print ("Error encountered while parsing file: ", fn)
            print (e)
            continue
        ext_features = np.hstack([mfccs, chroma, mel])
        features[index-1] = ext_features
        labels[index-1] = label
    return np.array(features), np.array(labels)


def parse_gyro_files(dir_path, label):
    """
    parse all sensor files of a class
    :param dir_path: path to class dir
    :param label: label of class
    """
    features, labels = np.empty((NUM_SAMPLES, NUM_GYRO_FEATURES*2)), np.empty(NUM_SAMPLES, dtype=str)

    for fn in os.listdir(dir_path+"/x"):
        index = int(fn.split('_')[-1].split('.')[0])
        if index > NUM_SAMPLES:
            continue
        xfile = os.path.join(os.path.join(dir_path, "x"), "gyrox"+fn[5:])
        yfile = os.path.join(os.path.join(dir_path, "y"), "gyroy"+fn[5:])
        try:
            xmfccs, xchroma, xmel = helper.extract_sensor_features(xfile)
            ymfccs, ychroma, ymel = helper.extract_sensor_features(yfile)
        except Exception as e:
            print (" Error encountered while parsing file: ", fn)
            print (e)
            continue

        ext_features = np.hstack([xmfccs, xchroma, xmel, ymfccs, ychroma, ymel])
        features[index-1] = ext_features
        labels[index-1] = label
    return np.array(features), np.array(labels)


def parse_files(parent_dir, file_types=None, exclude_dir=None):
    """
    loop thorough class in data dir and call corresponding fun for each type
    :param parent_dir:
    :param file_types:
    :param exclude_dir:
    :return: feature dict
    """
    features = {"audio": [np.empty((0, NUM_AUDIO_FEATURES)), np.empty(0)],
                "gyro": [np.empty((0, NUM_GYRO_FEATURES*2)), np.empty(0)],
                "acc": [np.empty((0, NUM_ACC_FEATURES)), np.empty(0)]}
    for class_dir in os.listdir(parent_dir):
        if exclude_dir and class_dir in exclude_dir:
            continue

        print ("process " + class_dir)

        if "audio" in file_types:
            audio_features, labels = parse_audio_files(os.path.join(os.path.join(parent_dir, class_dir),
                                                                    "audio"), str(class_dir))
            features["audio"][0] = np.vstack([features["audio"][0], audio_features])
            features["audio"][1] = np.append(features["audio"][1], labels)
        if "gyro" in file_types:
            gyro_features, labels = parse_gyro_files(os.path.join(os.path.join(parent_dir, class_dir),
                                                                  "gyro"), str(class_dir))
            features["gyro"][0] = np.vstack([features["gyro"][0], gyro_features])
            features["gyro"][1] = np.append(features["gyro"][1], labels)
        if "acc" in file_types:
            acc_features, labels = parse_acc_files(os.path.join(os.path.join(parent_dir, class_dir),
                                                                "acc"), str(class_dir))
            features["acc"][0] = np.vstack([features["acc"][0], acc_features])
            features["acc"][1] = np.append(features["acc"][1], labels)
    return features


def predict_with_prob(model, X):
    probs = model.predict_log_proba(X)
    Y = []
    for py in probs:
        index = py.argmax()
        Y.append((model.classes_[index], py[index]))
    return Y


class FusedModel(BaseEstimator):
    """
    A Classifier to combine various classifiers, using weights or offsets
    """

    def __init__(self, models=None, weights=None, mode="weight"):
        self.num_models = len(models)
        self.models = models
        self.mode = mode
        self.weights = weights

    def set_models(self, models):
        """
        set all models
        :param models: models
        """
        self.models = models

    def set_mode(self, weights, mode):
        """
        set mode and weights
        :param weights: list of weights
        :param mode: "weight" or "offset" or "append"
            weight -> will run both classifiers separately and will find the result based on a
            weighted average of their propablity of prediction
            offset ->
            append -> append both features and make a new feature vector

        """
        self.mode = mode
        self.weights = weights

    def _append_features(self, X):
        """
        append features
        :param X: list of feature set [[feature set 1], [feature set 2]]
        :return: appended feature set
        """
        X_new = [np.array([]) for x in X[0]]
        for feature_set in X:
            for i in range(len(feature_set)):
                np.append(list(feature_set[i]), X_new[i])
        return X_new

    def fit(self, X, Y, input_split=False):
        if self.mode == "append":
            X = self._append_features(X)
            Y = Y[0]
            self.models[0].fit(X, Y)
            return

        if input_split or self.mode == "append":
            initial_X = X
            X = [[] for x in initial_X[0]]
            for sample_tuple in initial_X:
                for i in range(len(sample_tuple)):
                    X[i].append(sample_tuple[i])
            initial_Y = Y
            Y = [[] for x in initial_Y[0]]
            for sample_tuple in initial_Y:
                for i in range(len(sample_tuple)):
                    Y[i].append(sample_tuple[i])

        for x, y, model in zip(X, Y, self.models):
            model.fit(x, y)

    def predict(self, X):
        """
        classify X
        :param X: sample list for each model, len(X) == len(model)
        :return: list of predicted classes
        """
        if self.mode != "append" and len(X) != self.num_models:
            raise Exception("models and data dimension mismatch")
        predicted_y = []

        if self.mode == "weight":

            X = [m.predict_log_proba(x) for m, x in zip(self.models, X)]
            predicted_x = []
            for sample_idx in range(len(X[0])):
                samples_per_model = []
                for model_idx in range(len(X)):
                    samples_per_model.append(X[model_idx][sample_idx])
                predicted_x.append(samples_per_model)

            for current_char_feature in predicted_x:
                # loop for each char
                y_tuples_weighted = []
                for class_idx in range(len(self.models[0].classes_)):
                    weighted_prob = 0
                    current_class = self.models[0].classes_[class_idx]
                    for model_idx in range(len(current_char_feature)):
                        weighted_prob += self.weights[model_idx]*current_char_feature[model_idx][class_idx]
                    y_tuples_weighted.append((current_class, weighted_prob))
                y_tuples_weighted.sort(key=lambda x: x[1], reverse=True)
                predicted_y.append(y_tuples_weighted[0][0])

        elif self.mode == "append":
            X = self._append_features(X)
            return self.models[0].predict(X)
        elif self.mode == "offset":
            X = [predict_with_prob(m, x) for m, x in zip(self.models, X)]
            predicted_x = []
            for sample_idx in range(len(X[0])):
                samples_per_model = []
                for model_idx in range(len(X)):
                    samples_per_model.append(X[model_idx][sample_idx])
                predicted_x.append(samples_per_model)
            for current_char_feature in predicted_x:
                y_prob_matrix = [(x[0], x[1]+w) for x, w in zip(current_char_feature, self.weights) ]
                y_prob_matrix.sort(key=lambda x: x[1], reverse=True)
                predicted_y.append(y_prob_matrix[0][0])
        else:
            raise Exception("mode not set")
        return predicted_y

    def score(self, X, Y, input_split=False):
        """
        test on X,Y and get accuracy
        :param X: list of samples for each model; len(X) == len(models)
        :param Y:  list of samples outputs for each model; len(X) == len(models)
        :param input_split: if len(x),len(x) == len(samples)
        :return: score
        """
        if input_split:
            initial_X = X
            X = [[] for x in X[0]]
            for sample_tuple in initial_X:
                for i in range(len(sample_tuple)):
                    X[i].append(sample_tuple[i])
            initial_Y = Y
            Y = [[] for x in initial_Y[0]]
            for sample_tuple in initial_Y:
                for i in range(len(sample_tuple)):
                    Y[i].append(sample_tuple[i])

        predicted_y = self.predict(X)

        final_score = sum([1 for g_y, p_y in zip(Y[0], predicted_y) if g_y == p_y])/float(len(predicted_y))
        # print final_score
        return final_score

    def crossval(self, X, Y, cv):
        """
        cross validate using this model
        :param X: list of samples for each model [ [for model1],[for model 1], ... ]
        :param Y: list of samples outputs for each model [ [for model1],[for model 1], ... ]
        :param cv: number of folds
        :return: list
        """
        cv_len = len(X[0])/cv
        final_prob = []

        for x, y in zip(X, Y):
            np.random.seed(0)
            np.random.shuffle(x)
            np.random.seed(0)
            np.random.shuffle(y)

        for k in range(cv):
            x_splitted_test = []
            y_splitted_test = []
            x_splitted_train = []
            y_splitted_train = []
            for i in range(len(self.models)):
                x_splitted_test.append(X[i][k*cv_len: (k+1)*cv_len])
                y_splitted_test.append(Y[i][k*cv_len: (k+1)*cv_len])
                x_splitted_train.append(list(X[i][:k*cv_len]) + list(X[i][(k+1)*cv_len:]))
                y_splitted_train.append(list(Y[i][:k*cv_len]) + list(Y[i][(k+1)*cv_len:]))
            self.fit(x_splitted_train, y_splitted_train)
            final_prob.append(self.score(x_splitted_test, y_splitted_test))
        return final_prob


def main():

    # extract features
    # all_data = parse_files(TRAIN_DATA_DIR, ["audio", "acc", "gyro"], exclude_dir=['pcms'])

    # joblib.dump(all_data, "data_set2_stacked_all.sav")
    # joblib.dump(all_data, "parsed_features_stacked_all.sav")
    all_data = joblib.load("data_set2_stacked_all.sav")

    gfeatures, glabels = all_data['gyro']
    afeatures, alabels = all_data['audio']
    cfeatures, clabels = all_data['acc']

    if MODE == 'cross':

        # split train and test for audio samples
        rand_seed = 4
        np.random.seed(rand_seed)
        atrain_test_split = np.random.rand(len(afeatures)) < 0.75
        atrain_x = afeatures[atrain_test_split]
        atrain_y = alabels[atrain_test_split]
        atest_x = afeatures[~atrain_test_split]
        atest_y = alabels[~atrain_test_split]

        # split train and test for gyro samples
        np.random.seed(rand_seed)
        gtrain_test_split = np.random.rand(len(gfeatures)) < 0.75
        gtrain_x = gfeatures[gtrain_test_split]
        gtrain_y = glabels[gtrain_test_split]
        gtest_x = gfeatures[~gtrain_test_split]
        gtest_y = glabels[~gtrain_test_split]

    else:
        test_data = joblib.load("tmp_test.sav")

        tgfeatures, tglabels = test_data['gyro']
        tafeatures, talabels = test_data['audio']
        tcfeatures, tclabels = test_data['acc']

        atrain_x = afeatures
        atrain_y = alabels
        atest_x = tafeatures
        atest_y = talabels

        gtrain_x = gfeatures
        gtrain_y = glabels
        gtest_x = tgfeatures
        gtest_y = tglabels

    # preprocess
    ascaler, gscaler = StandardScaler(), StandardScaler()
    ascaler.fit(atrain_x)
    gscaler.fit(gtrain_x)
    atrain_x = ascaler.transform(atrain_x)
    atest_x = ascaler.transform(atest_x)
    gtrain_x = gscaler.transform(gtrain_x)
    gtest_x = gscaler.transform(gtest_x)


    # train
    amodel = LogisticRegression()
    gmodel = LogisticRegression()
    cmodel = LogisticRegression()
    fmodel = FusedModel([LogisticRegression(), LogisticRegression()], [0.9, 0.1], "weight")
    # model = DecisionTreeClassifier(max_depth=3)  # 69
    # model = GaussianNB()
    # model = svm.SVC(kernel='linear', C=1.0)
    # model = svm.SVC(kernel='poly', degree=3, C=1.0)
    # model = svm.SVC(kernel='rbf', gamma=0.7, C=1.0)

    amodel.fit(atrain_x, atrain_y)
    gmodel.fit(gtrain_x, gtrain_y)
    fmodel.fit([atrain_x, gtrain_x], [atrain_y, gtrain_y])

    # predict
    print("fused", fmodel.score([atest_x, gtest_x], [atest_y, gtest_y]) )
    print ("cross fused", np.array(fmodel.crossval([afeatures, gfeatures, cfeatures], [alabels, glabels, clabels], 5)).mean())
    #
    print ("audio", amodel.score(atest_x, atest_y))
    print ("cross audio", cross_val_score(amodel, afeatures, alabels, cv=3).mean())
    #
    print ("gyro", gmodel.score(gtest_x, gtest_y))
    print ("cross gyro", cross_val_score(gmodel, gfeatures, glabels, cv=3).mean())
    #
    # print ("acc", cmodel.score(ctest_x, ctest_y))
    # print ("cross acc", cross_val_score(cmodel, cfeatures, clabels, cv=3).mean())

    # analyze
    # print (confusion_matrix(test_y, predicted_y))
    # print (metrics.classification_report(test_y, predicted_y))


if __name__ == '__main__':
    main()
