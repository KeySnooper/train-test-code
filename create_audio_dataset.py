import librosa
import numpy as np
import scipy
import os, sys

AFILE = '../data/alpha/'
FILES_NUM = 31
DATASET_PATH = '../data/alpha/dataset'
TEXT = "qwertyuiopasdfghjklzxcvbnm "

KEY_WIDTH = 0.36
SPEC_THRESHOLD = 1e-06


def get_peak_clips_spec(y, sr):
    a = scipy.signal.spectrogram(y, sr)
    spec_acc = np.sum(a[2], axis=0)
    t = a[1]
    tt = [float(t[np.where(spec_acc == y)]) for y in [x for x in spec_acc if x > SPEC_THRESHOLD]]
    clips = []
    while len(tt) > 0:
        clips.append((tt[0], tt[0]+KEY_WIDTH))
        tt = filter(lambda i: i > tt[0]+KEY_WIDTH, tt)
    return clips


def get_peak_clips_time(y, sr):
    pass


def get_labeled_set(y, sr, text, clips):
    labeled_data = []
    # print("number of text sets present : %d", len(clips)/len(text))
    print("number of clips present : %d", len(clips))
    for i, clip in enumerate(clips):
        labeled_data.append((text[i % len(text)], y[int(sr*clip[0]):int(sr*clip[1])]))
    return labeled_data


def write_dataset(labeled_data, sr):
    for label, clip in labeled_data:
        dir_path = os.path.join(DATASET_PATH, label)
        if not os.path.isdir(dir_path):
            os.makedirs(dir_path)
        num = str(len([name for name in os.listdir(dir_path) if os.path.isfile(os.path.join(dir_path, name))]))
        librosa.output.write_wav(os.path.join(dir_path, num+'.wav'), clip, sr)


def main():
    if len(sys.argv) > 1:
        y, sr = librosa.load(AFILE + "./t/fast.mp3")
        # y, sr = librosa.load(AFILE + "t" + sys.argv[1] + ".mp3")
        time_clips = get_peak_clips_spec(y, sr)
        print (len(time_clips))
    else:
        for i in range(1, FILES_NUM):
            print ("file "+str(i))
            y, sr = librosa.load(AFILE+"t"+str(i)+".mp3")
            time_clips = get_peak_clips_spec(y, sr)
            labeled_data = get_labeled_set(y, sr, TEXT, time_clips)
            write_dataset(labeled_data, sr)


if __name__ == '__main__':
    main()